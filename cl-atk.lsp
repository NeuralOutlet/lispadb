
;;; ---------------------------------------- ;;;
;;; Functions often used in conjunction with ;;;
;;; Android Debug Bridge (ADB) commands      ;;;
;;; ---------------------------------------- ;;;

;; Set up package
(defpackage :atk
	(:use :common-lisp)
	(:export :aapt))
(in-package :atk)


;; Function for utilising aapt options,including
;; dump badging and others - various return types

;; Android Asset Packaging Tool
(defun aapt (filepath)
	(let (out err ret)
		(setf (values out err ret) 
			(uiop:run-program (concatenate 'string "aapt dump badging" " " filepath)))
		(list out err ret)))
