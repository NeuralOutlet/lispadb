;;; This is where all the ADB tests are run from ;;;

;; Get commandline arguments
;; for multiple interpretors
(defun get-command-line-args ()
	(or 
		#+SBCL *posix-argv*
		#+CLISP *args*
		#+ABCL *command-line-argument-list*
		#+ECL (si::command-args)
		nil))
(defvar *arg-lst* (get-command-line-args))

;; shorthand
(defun is-in (str lst)
	(consp (member str lst :test #'equal)))

;; Global variables
(defvar *negative-tests*
	(if (is-in "--negative" *arg-lst*) t nil))

;; Load in ASDF3
(load "~/common-lisp/asdf/build/asdf")

;; Load in ADB
(load "../cl-adb.lsp")

;; A function for verifying test result based
;; on the :default data-return type (string if
;; successful, cons cell if unsuccessful).
(defun verify (result)
	(format t "~a~%" (if *negative-tests*
		(if (consp result) "PASSED" "FAILED")
		(if (consp result) "FAILED" "PASSED"))))

;; Begin by killing server
(format t "adb kill-server~%~a" (adb:kill-server))

;; Run tests by loading the test files
(load "adb-tests/push-pull.test")

;; Interactive tests (user-required)
(if (is-in "--interactive" *arg-lst*)
	(load "adb-tests/shell.test"))

;; Positive only tests
(if (not *negative-tests*)
	(progn
		(load "adb-tests/devices.test")
		(load "adb-tests/connections.test")))
