
;; Load in ASDF3
(load "~/common-lisp/asdf/build/asdf")

;; Load in ATK
(load "../cl-atk.lsp")

;; Load in tests
(load "atk-tests/aapt.test")
