
;; See if we can load in uiop
(load "asdf-uiop.lisp")
(uiop:run-program "ls" :output :interactive)

;; Try an adb command on device
(load "cl-adb.lsp")
(setf *inside-shell* t)
(defvar val-2 (adb:shell "dumpsys power"))
(format t "~a~%" val-2)
