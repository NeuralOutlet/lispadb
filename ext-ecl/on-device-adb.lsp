

(load "~/common-lisp/asdf/build/asdf.lisp")
(load "../cl-adb.lsp")

(format t "Dumpsys from host~%")
(defvar val (adb:shell "dumpsys power"))
(format t "~a~%" val)

(print (adb:push "asdf.lisp" "/data/local/tmp/"))
(print (adb:push "ecl-pie" "/data/local/tmp/"))
(print (adb:push "test.lsp" "/data/local/tmp/"))
(print (adb:push "../cl-adb.lsp" "/data/local/tmp"))

(uiop:run-program "adb shell \"cd /data/local/tmp ; chmod 755 ecl-pie\ ; ./ecl-pie -q -norc -shell test.lsp\"" :output :interactive)

