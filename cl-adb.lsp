;;; ---------------------------------------------- ;;;
;;; Functions for using Android Debug Bridge (ADB) ;;;
;;; ---------------------------------------------- ;;;

;; Set up package
(defpackage :adb
	(:use :common-lisp)
	(:shadow :push)
	(:export :shell :open-shell
	         :connect :disconnect
	         :kill-server
	         :devices
	         :push :pull))
(in-package :adb)

;; Global variables
(defvar *inside-shell* nil)

;; Generic adb command, returns:
;; ((output) (error) returncode)
(defun adb (command &key (adbout :string) (adberr :string) (adbwait t))
	(let (cmd out err ret adbvar)
		(setf adbvar (if *inside-shell* "" "adb"))
		(setf cmd (concatenate 'string adbvar " " command))
		(setf (values out err ret)
			(uiop:run-program cmd :output adbout :error-output adberr :ignore-error-status t :wait adbwait))
		(list out err ret)))

;; Generic return function, to be paired with
;; adb command when making subsequent commands.
;; Parses the list output given by adb.
(defun adb-return (vals data-return)
	(case data-return
		(:full vals)
		(:adbout (nth 0 vals))
		(:adberr (nth 1 vals))
		(:adbret (nth 2 vals))
		(:default (if (equal (nth 2 vals) 0)
			(nth 0 vals)
			(list (nth 1 vals) (nth 2 vals))))))

;; Open adb shell interactively
;; returns exitcode only
(defun open-shell ()
	(let (out err ret)
		(setf (values out err ret)
			(uiop:run-program "adb shell" :output :interactive :ignore-error-status t :wait t))
		ret))

;; Run remote commands via adb shell
(defun shell (cmd &key (data-return :default))
	(let ((vals '()) (shellvar ""))
		(setf shellvar (if *inside-shell* "" "shell"))
		(setf vals (adb (concatenate 'string shellvar " " cmd)))
		(adb-return vals data-return)))

(defun kill-server (&key (data-return :default))
	(let ((vals '()))
		(setf vals (adb "kill-server"))
		(adb-return vals data-return)))

;; adb-connect
(defun connect (host &optional port &key (data-return :default))
	(let ((vals '()) host-port)
		(if (not (null port))
			(setf host-port (concatenate 'string host ":" (string port)))
			(setf host-port host))
		(setf vals (adb (concatenate 'string "connect" " " host-port)))
		(adb-return vals data-return)))

;; adb-disconnect
(defun disconnect (&optional host port &key (data-return :default))
	(let ((vals '()) host-port)
		(if (not (null port))
			(setf host-port (concatenate 'string host ":" (string port)))
			(setf host-port host))
		(setf vals (adb (concatenate 'string "disconnect" " " host-port)))
		(adb-return vals data-return)))

;; adb-devices
(defun devices (&optional flag &key (data-return :default))
	(let ((vals '()))
		(setf vals (adb (concatenate 'string "devices" " " flag)))
		(adb-return vals data-return)))

;; adb-push/pull should have :swap key which
;; is default true, the error/output are piped
;; to the wrong place for some reason.

;; adb-push
(defun push (file path &key (data-return :default) (swap t))
	(let ((tmp '()) (vals '()))
		(setf tmp (adb (concatenate 'string "push" " " file " " path)))
		(setf vals (if swap
			(list (nth 1 tmp) (nth 0 tmp) (nth 2 tmp))
			tmp))
		(adb-return vals data-return)))

;; adb-pull
(defun pull (filepath &key (data-return :default) (swap t))
	(let ((tmp '()) (vals '()))
		(setf tmp (adb (concatenate 'string "pull" " " filepath)))
		(setf vals (if swap
			(list (nth 1 tmp) (nth 0 tmp) (nth 2 tmp))
			tmp))
	(adb-return vals data-return)))
