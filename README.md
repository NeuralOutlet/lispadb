# LispADB #

Some ease-of-use functions for scripting with Common Lisp and Android Debug Bridge (ADB). Tested against clisp, sbcl, abcl, and ecl.

### Linux Install of Interpretors ###

* CLISP: sudo apt-get install clisp
* SBCL: sudo apt-get install sbcl
* ECL: sudo apt-get install ecl

Hard right?

* ABCL (requires JDK):
    * Download source from abcl.org
    * untar and go to folder with INSTALL file (read it if you want)
    * $ ant # Run the ant command and it'll make abcl binary
    * sudo cp abcl /usr/bin/

* ECL-Android (building requires Android SDK/NDK):
    * git clone https://github.com/ageneau/ecl-android.git
    * Go to folder with INSTALL file (read it if you want)
    * set $ANDROID_SDK_ROOT and $ANDROID_NDK_ROOT
    * make update
    * make android # for ARM or
    * make androidx86
    * adb push to device, adb shell and run it

Pre-built binary for ARM-based Android devices: https://github.com/YasuakiHonda/ECL-Precompiled-Binaries

### Running the test ###

Go to 'tests' folder then run one of the lsp files with an interpretor:

* ABCL: abcl --noinform --noinit --nosystem --load adb-tests.lsp
* ECL: ecl -q -norc -shell adb-tests.lsp
* SBCL: sbcl --script adb-tests.lsp
* CLISP: clisp adb-tests.lsp


### ASDF? ###

* git clone https://gitlab.common-lisp.net/asdf/asdf.git
* cd asdf/build/
* make # (should create a bin folder with asdf files)
* cp -r asdf/ ~/common-lisp/ # (or put it wherever and change path in test.lsp)

Note - SBCL, ABCL, and others come with ASDF already in them. Find out more at: https://common-lisp.net/project/asdf/